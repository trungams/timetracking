<?php
include_once ('../../system/libraries/Migration.php');

class Migration_Version_115 extends CI_Migration
{
	function __construct()
	{
		parent::__construct();
	}

	public function up()
	{
		$this->db->query("ALTER TABLE `tbl_task` ADD `current_assignee` text COLLATE utf8_unicode_ci, AFTER `permission`;");

	}
}