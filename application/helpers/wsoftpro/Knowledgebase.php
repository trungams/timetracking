<?php
namespace WsoftPro\helpers\Wsoftpro;

include_once 'AbstractHelpers.php';

class Knowledgebase extends \WsoftPro\helpers\Wsoftpro\AbstractHelpers
{

    /**@var \Kb_model**/
    protected $_kb_model;
    /**
     * Construct
     *
     **/
    public function __construct($device = false)
    {
        parent::__construct($device);
        $this->_kb_model = $this->getModel('Kb');
    }

    /**
     * Function get Knowledgbase detail
     *
     * @param $id
     * @return bool | \stdClass
    **/
    public function getKnowledgebaseInfo($id)
    {
        if($id){
            $this->_kb_model->increase_total_view($id);
            return $this->_kb_model->check_by(array('kb_id' => $id), 'tbl_knowledgebase');
        }

        return false;
    }

}