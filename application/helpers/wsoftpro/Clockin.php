<?php
namespace WsoftPro\helpers\Wsoftpro;

include_once 'AbstractHelpers.php';

class Clockin extends \WsoftPro\helpers\Wsoftpro\AbstractHelpers
{
    /**@var \Attendance_Model**/
    protected $_attendance_model;
    /**
     * Construct
     *
     **/
    public function __construct($device = false)
    {
        parent::__construct($device);
        $this->_attendance_model = $this->getModel('Attendance');
    }

    public function checkClockIn($attendance_id,$user_id)
    {
        $clockinData = $this->_attendance_model->findCurrentClockIn($attendance_id,$user_id);
        if($clockinData && !empty($clockinData)){
            return $clockinData[0];
        }

        return false;
    }


}