<?php
namespace WsoftPro\helpers\Wsoftpro;

include_once 'AbstractHelpers.php';

class Timetracking extends \WsoftPro\helpers\Wsoftpro\AbstractHelpers
{
    const XML_TIME_START = '09:00:00';
    const XML_TIME_STOP = '18:00:00';
    protected $timeWorking;

    /**
     * @var \WsoftPro\helpers\Wsoftpro\Date
    **/
    protected $_date;

    /**
     * @var \Attendance_Model
    **/
    protected $attendance_model;

    /**
     * @var \Global_Model
    **/
    protected $global_model;

    /**
     * @return Date
     */
    public function getDate()
    {
        return $this->_date;
    }

    /**
     * @param Date $date
     */
    public function setDate($date)
    {
        $this->_date = $date;
    }

    /**
     * @return \Attendance_Model
     */
    public function getAttendanceModel()
    {
        return $this->attendance_model;
    }

    /**
     * @param \Attendance_Model $attendance_model
     */
    public function setAttendanceModel($attendance_model)
    {
        $this->attendance_model = $attendance_model;
    }

    public function __construct($device = false)
    {
        parent::__construct($device);
        $this->_date = $this->getHelper('Date');
        $this->attendance_model = $this->getModel('Attendance');
        $this->global_model = $this->getModel('Global');
    }

    public function getFineAmount($attendace_info,$user_id)
    {
        $totalFine = 0;
        $total_fine_time_late = 0;
        $total_leave_day = 0;
        $total_leave_active_day = 0;
        $totalAmountLeave = 0;
        $allDayLeave = $this->getLeaveCalculatorSalary($user_id);
        foreach ($attendace_info as $week=>$dateWeek){
            foreach ($dateWeek as $day=>$dataDay){
                $dataAttendance = null;
                $dataAttendances = $dataDay[$user_id];
                if(is_array($dataAttendances) && !empty($dataAttendances)){
                    $dataAttendance = $dataAttendances[0];
                }
                $dateTotalHours = $this->getTotalHoursWorked($day,$dataDay,$user_id);
                $total_hh = $dateTotalHours['total_hh'];
                $total_mm = $dateTotalHours['total_mm'];
                $holiday = $dateTotalHours['holiday'];
                $leave = $dateTotalHours['leave'];
                $absent = $dateTotalHours['absent'];
                $missingClockOut = $dateTotalHours['clock_error'];
                $parttime = $dateTotalHours['parttime'];
                $attendance_id = $dataAttendance->attendance_id;
                $timeLate = false;
                $firstTimeClockIn = false;
                $checkLeaveCalculatorSalary = $this->checkLeaveCalculatorSalary($day,$allDayLeave);
                if(!$timeLate){
                    $firstTimeClockIn = $this->getFirstTimeClockin($attendance_id);
                    $timeLate = $this->getTimeLaterInDay($day,$attendance_id,$firstTimeClockIn);
                }
                $checkShow = false;
                if(!$holiday){
                    if(
                        $missingClockOut || $timeLate['hours'] > 0 || $timeLate['mins']
                        || $leave || $absent || !$firstTimeClockIn
                    ){
                        if($timeLate['hours'] > 0){
                            $total_fine_time_late += 50000;
                        }else{
                            if((int)$timeLate['mins'] > 0){
                                if($timeLate['mins'] > 30){
                                    $total_fine_time_late += 50000;
                                }else{
                                    $total_fine_time_late += (int)$timeLate['mins'] * 1000;
                                }
                            }
                        }
                        $checkShow = true;
                    }else{
                        if($total_hh != 0 || $total_mm != 0){
                            if($parttime){
                                $checkShow = true;
                            }
                        }
                    }
                }
                if($checkShow){
                    if ($total_mm > 60) {
                        $total_hh += intval($total_mm / 60);
                        $total_mm = intval($total_mm % 60);
                    }

                    if ($missingClockOut){
                        $total_fine_time_late += 50000;
                    }elseif($total_hh != 0 || $total_mm != 0) {
                        if($parttime){
                            $total_leave_active_day += 0.5;
                        }
                    } elseif ($leave) {
                        if(!$checkLeaveCalculatorSalary){
                            $total_leave_active_day ++;
                        }
                    }
                    $holiday = NULL;
                    $leave = NULL;
                    $absent = NULL;
                    if(!$firstTimeClockIn){
                        if(!$checkLeaveCalculatorSalary){
                            $total_leave_day ++;
                        }
                    }
                }
            }
        }
        $totalFine +=  $total_fine_time_late;
        $baseSalaryInDay = $this->getBaseAmoutInAday($user_id,$day);
        if($baseSalaryInDay !== false && $total_leave_active_day > 0){
            $totalAmountLeaveActive = $total_leave_active_day * $baseSalaryInDay;
            $totalFine += $totalAmountLeaveActive;
        }
        if($baseSalaryInDay !== false && $total_leave_day > 0){
            $totalAmountLeave = $total_leave_day * $baseSalaryInDay;
            $totalFine += $totalAmountLeave;
        }

        return $totalFine;
    }

    public function checkLeaveCalculatorSalary($date,$allDayLeave)
    {
        $date = $this->_date->formatDate('Y-m-d',$date);

        if(in_array($date,$allDayLeave)){
            return true;
        }

        return false;
    }

    public function getLeaveCalculatorSalary($user_id)
    {
        $data = array();
        /**@var LeaveManager $leaveManager**/
        $leaveManager = $this->getHelper('LeaveManager');
        $allDayLeave = $leaveManager->getTotalLeaveSalary($user_id);
        foreach ($allDayLeave as $leave){
            if(isset($leave->leave_start_date)){
                $data[] = $leave->leave_start_date;
            }
        }

        return $data;
    }

    public function getSalaryTemplateInforByUser($id)
    {
        if($id){
            $sql = 'SELECT tbl_employee_payroll.*,tbl_salary_template.* FROM tbl_employee_payroll
                JOIN tbl_salary_template ON tbl_salary_template.salary_template_id = tbl_employee_payroll.salary_template_id
                WHERE tbl_employee_payroll.user_id = '.$id;
            return $result = $this->getFirstRow($sql);
        }

        return false;
    }

    public function getBaseSalaryByUserId($id)
    {
        $info = $this->getSalaryTemplateInforByUser($id);
        if($info !== false && is_object($info)){
            return $info->basic_salary;
        }

        return false;
    }

    public function getPrivateHoliday($date)
    {
        $month = $this->_date->getMonth()->getFullMonthNumber($date);
        $year = $this->_date->getYear()->getFullYearNumber($date);
        $allHoliday = $this->getHolidayInMonth($month,$year);
        $data = array();
        if($allHoliday){
            foreach ($allHoliday as $obj){
                if(isset($obj->calculator_salary) && $obj->calculator_salary == 1){
                    $data[strtotime($obj->start_date)] = $obj->start_date;
                }
            }
        }
        return $data;
    }
    
    public function getBaseAmoutInAday($user_id,$date)
    {
        $baseSalary = $this->getBaseSalaryByUserId($user_id);
        $month = $this->_date->getMonth()->getFullMonthNumber($date);
        $year = $this->_date->getYear()->getFullYearNumber($date);
        $allday = $this->getDataDaysWorkOnMonth($month,$year);
        $numberDaysWorking = count($allday) + count($this->getPrivateHoliday($date));
        if($allday && is_array($allday)){
            return $baseSalary / $numberDaysWorking;
        }

        return false;
    }

    public function getEmployeeByDeptAndUserId($departments_id,$user_id)
    {
        try{
            return $this->attendance_model->getEmployeeByDeptAndUserId($departments_id,$user_id);
        }catch (\Exception $e){
            $this->logMessage($e->getMessage());
        }

        return false;
    }

    /**
     * Function get Time Working in a day
     * @param mixed $timeWorking
     * @return array
    **/
    public function getTimeWorking($timeWorking = null)
    {
        $data = array('start'=>self::XML_TIME_START,'end'=>self::XML_TIME_STOP);
        if(!$this->timeWorking){
            $dataTimeWork = $this->attendance_model->getTimeWorking();
            if($dataTimeWork){
                $this->timeWorking = array_merge($data,$dataTimeWork);
            }
        }

        if($timeWorking && isset($this->timeWorking[$timeWorking])){
            $data = $this->timeWorking[$timeWorking];
        }

        return $data;
    }

    /**
     * Function get Time late in a day
     * @param string $day
     * @param mixed $attendance_id
     * @param mixed $firstTimeClockIn
     * @return array
    **/
    public function getTimeLaterInDay($day,$attendance_id,$firstTimeClockIn = null)
    {
        if(!$firstTimeClockIn){
            $firstTimeClockIn = $this->getFirstTimeClockin($attendance_id);
        }

        $dayWeek = $this->_date->getDay()->getDayWeekFullName($day);
        $timeWorking = $this->getTimeWorking($dayWeek);
        return $this->_date->getDiffDate($firstTimeClockIn->clockin_time,$timeWorking['start']);
    }

    /**
     * Function get Time Later by attendance_id
     * @param $attendance_id
     * @return \stdClass | bool
    **/
    public function getFirstTimeClockin($attendance_id)
    {
        $result = false;
        try{
            if($attendance_id){
                $sql = 'SELECT attendance.attendance_id,attendance.user_id,attendance.leave_category_id,attendance.clocking_status,
                 device.device_osx,device.device_name,
                 clock.clock_id,clock.clockin_time,clock.clockout_time,clock.ip_address
                FROM tbl_attendance as attendance 
                JOIN user_device as device ON attendance.attendance_id = device.attendance_id
                JOIN tbl_clock as clock ON clock.clock_id = device.clock_id
                WHERE attendance.attendance_id = '.$attendance_id;
                $result = $this->getFirstRow($sql);
            }
        }catch (\Exception $e){
            $this->logMessage($e->getMessage());
        }

        return $result;
    }

    /**
     * Function get total hours worked in a day
     * @param string $date
     * @param mixed $attendace
     * @param mixed $user_id
     *
     * @return array
    **/
    public function getTotalHoursWorked($date,$attendace,$user_id)
    {
        $total_hh = 0;
        $total_mm = 0;
        $holiday = false;
        $leave = false;
        $absent = false;
        $missingClockOut = false;
        $parttime = false;
        foreach ($attendace as $key => $v_attendace) {
            if ($key == $user_id) {
                if (!empty($v_attendace)) {
                    foreach ($v_attendace as $v_attandc) {
                        if (!empty($v_attandc->clockout_time)) {
                            if($v_attandc->date_in !== $v_attandc->date_out){
                                $missingClockOut = true;
                            }
                            // calculate the start timestamp
                            $startdatetime = $v_attandc->date_in . " " . $v_attandc->clockin_time;
                            // calculate the end timestamp
                            $enddatetime = $v_attandc->date_out . " " . $v_attandc->clockout_time;
                            // calulate the difference in seconds
                            $dateDateDiff = $this->getDate()->getDiffDate($enddatetime, $startdatetime);
                            $date = '';
                            $total_mm += $dateDateDiff['mins'];
                            $total_hh += $dateDateDiff['hours'];
                        } elseif ($v_attandc->date == $date && $v_attandc->attendance_status == 'H') {
                            $holiday = true;
                        } elseif ($v_attandc->attendance_status == '3') {
                            $leave = true;
                        } elseif ($v_attandc->attendance_status == '0') {
                            $absent = true;
                        }else{
                            $missingClockOut = true;
                        }
                    }
                }
            }
        }
        if(!$total_hh && !$total_mm){
            $missingClockOut = false;
            $absent = true;
        }else{
            if($total_hh < 7){
                $parttime = true;
            }
        }
        $data = array();
        $data['total_hh'] = $total_hh;
        $data['total_mm'] = $total_mm;
        $data['holiday'] = $holiday;
        $data['leave'] = $leave;
        $data['absent'] = $absent;
        $data['clock_error'] = $missingClockOut;
        $data['parttime'] = $parttime;
        return $data;
    }

    /**
     * get data Employee flow by department
     *
     * @param mixed $departmentId
     * @return array
    **/
    public function getEmployeeIdByDepartmentId($departmentId)
    {
        return $this->getAttendanceModel()->get_employee_id_by_dept_id($departmentId);
    }

    /**
     * get all holiday working day
     * object tbl_working_days
     *
     * @return array
     **/
    public function getHolidays()
    {
        return $this->global_model->get_holidays(); //tbl working Days Holiday
    }


    /**
     * Get all public holiday in month
     * @param string $date
     * @return array
    **/
    public function getPublicHolidaysInMonth($date)
    {
        $yymm = $this->_date->formatDate('Y-m',$date);
        $public_holiday = $this->global_model->get_public_holidays($yymm);
        $p_hday = array();
        //tbl a_calendar Days Holiday
        if (!empty($public_holiday)) {
            foreach ($public_holiday as $p_holiday) {
                try{
                    if($p_holiday->event_name == 1){
                        $p_hday[] = $this->attendance_model->GetDays($p_holiday->start_date, $p_holiday->end_date);
                    }
                }catch (\Exception $e){
                    $this->logMessage($e->getMessage());
                }
            }
        }

        return $p_hday;
    }

    /**
     * Get data Attendace by month flow by employee info
     * @param $date
     * @param $employees_info
     *
     * @return array
    **/
    public function getDataAttendace($date,$employees_info)
    {
        $data = array();
        $dataWeekInfo = array();
        $dataAttendace = array();
        $month = $this->_date->getMonth()->getFullMonthNumber($date);
        $year = $this->_date->getYear()->getFullYearNumber($date);
        $holidays = $this->getHolidays(); //tbl working Days Holiday
        $p_hday = $this->getPublicHolidaysInMonth($date);
        $num = $this->_date->getMonth()->getDateNumberInMonth($date);
        $yymm = $this->_date->formatDate('Y-m', $date);
        foreach ($employees_info as $sl => $v_employee) {
            $key = 1;
            $x = 0;
            for ($i = 1; $i <= $num; $i++) {

                if ($i >= 1 && $i <= 9) {
                    $sdate = $yymm . '-' . '0' . $i;
                } else {
                    $sdate = $yymm . '-' . $i;
                }
                $day_name = date('l', strtotime("+$x days", strtotime($year . '-' . $month . '-' . $key)));

                $dataWeekInfo[date('W', strtotime($sdate))][$sdate] = $sdate;

                // get leave info
                if (!empty($holidays)) {
                    foreach ($holidays as $v_holiday) {
                        if ($v_holiday->day == $day_name) {
                            $flag = 'H';
                        }
                    }
                }
                if (!empty($p_hday)) {
                    foreach ($p_hday as $v_hday) {
                        if ($v_hday[0] == $sdate) {
                            $flag = 'H';
                        }
                    }
                }
                if (!empty($flag)) {
                    $dataAttendace[date('W', strtotime($sdate))][$sdate][$v_employee->user_id] = $this->attendance_model->attendance_report_by_empid($v_employee->user_id, $sdate, $flag);
                } else {
                    $dataAttendace[date('W', strtotime($sdate))][$sdate][$v_employee->user_id] = $this->attendance_model->attendance_report_by_empid($v_employee->user_id, $sdate);
                }
                $key++;
                $flag = '';
            }
        }
        $data['attendace_info'] = $dataAttendace;
        $data['week_info'] = $dataWeekInfo;

        return $data;
    }
    

    /**
     * Function get all day active working in month
     * @param mixed $month
     * @param mixed $year
     *
     * @return array
    **/
    public function getDataDaysWorkOnMonth($month,$year){
        $daysWork = array();
        $stringDate = $year.'-'.$month.'-01';
        $days = $this->_date->getMonth()->getDateNumberInMonth($stringDate);
        $holiday = $this->getAllDayHolidayStaturdayInMonth($month,$year);
        for($i = 1; $i <= $days ; $i++){
            $date = $year.'-'.$month.'-'.$i;
            $dayStrtotime = strtotime($date);
            if(!$this->_date->getDay()->isSunday($date)){
                if(!array_key_exists($dayStrtotime,$holiday)){
                    $daysWork[date ( 'd-m-Y' ,strtotime($date))] = date ( 'd-m-Y' ,strtotime($date));
                }
            }
        }
        return $daysWork;
    }

    /**
     * Function get all day saturday still working on month
     * @param mixed $month
     * @param mixed $year
     *
     * @return array
     **/
    public function getAllDayWorkStaturdayInMonth($month,$year){
        $sql = "SELECT start_date FROM tbl_holiday WHERE event_name like 'Làm' AND YEAR(start_date) = ? AND   MONTH(start_date) = ?";
        $result = $this->getAllResult($sql,array($year,$month));
        $data = array();
        if($result){
            foreach ($result as $obj){
                $data[strtotime($obj->start_date)] = $obj->start_date;
            }
        }
        return $data;
    }

    public function getHolidayInMonth($month,$year)
    {
        $sql = "SELECT * FROM tbl_holiday WHERE event_name like 'Nghỉ' AND YEAR(start_date) = ? AND   MONTH(start_date) = ?";
        $result = $this->getAllResult($sql,array($year,$month));

        return $result;
    }

    /**
     * Function get all day holiday event in month
     * @param mixed $month
     * @param mixed $year
     *
     * @return array
    **/
    public function getAllDayHolidayStaturdayInMonth($month,$year){

        $result = $this->getHolidayInMonth($month,$year);
        $data = array();
        if($result){
            foreach ($result as $obj){
                $data[strtotime($obj->start_date)] = $obj->start_date;
            }
        }
        return $data;
    }
}