<?php
namespace WsoftPro\helpers\Wsoftpro;

include_once 'AbstractHelpers.php';

class Date extends \WsoftPro\helpers\Wsoftpro\AbstractHelpers
{
    const XML_PATH_DATE = 'Date/';

    /**@var \WsoftPro\helpers\Wsoftpro\Date\Month **/
    protected $_month;

    /**@var \WsoftPro\helpers\Wsoftpro\Date\Day **/
    protected $_day;

    /**@var \WsoftPro\helpers\Wsoftpro\Date\Year **/
    protected $_year;

    /**
     * Construct
     *
     **/
    public function __construct($device = false)
    {
        parent::__construct($device);
    }

    /**
     * function get data diff date
     * @param string date
     * @param string $compareDate
     *
     * @return array
    **/
    public function getDiffDate($date,$compareDate)
    {
        $data = array();
        $data['year'] = $this->getDiffDateYear($date,$compareDate);
        $data['days'] = $this->getDiffDateDay($date,$compareDate);
        $data['hours'] = $this->getDiffDateHours($date,$compareDate);
        $data['mins'] = $this->getDiffDateMins($date,$compareDate);

        return $data;
    }

    /**
     * function get time diff date
     * @param string date
     * @param string $compareDate
     *
     * @return int
     **/
    public function getTimeDiffDate($date,$compareDate)
    {
        $date = $this->dateToTime($date);
        $compareDate = $this->dateToTime($compareDate);
        return $date - $compareDate;
    }

    /**
     * function get year diff date
     * @param string date
     * @param string $compareDate
     *
     * @return int
     **/
    public function getDiffDateYear($date,$compareDate)
    {
        $difference = $this->getTimeDiffDate($date,$compareDate);
        $data = 0;
        if($difference > $data){
            $data = abs(floor($difference / 31536000));
        }

        return $data;
    }

    /**
     * function get day diff date
     * @param string date
     * @param string $compareDate
     *
     * @return int
     **/
    public function getDiffDateDay($date,$compareDate)
    {
        $difference = $this->getTimeDiffDate($date,$compareDate);
        $years = $this->getDiffDateYear($date,$compareDate);
        $data = 0;
        if($difference > $data){
            $data = abs(floor(($difference - ($years * 31536000)) / 86400));
        }

        return $data;
    }

    /**
     * function get hours diff date
     * @param string date
     * @param string $compareDate
     *
     * @return int
     **/
    public function getDiffDateHours($date,$compareDate)
    {
        $difference = $this->getTimeDiffDate($date,$compareDate);
        $years = $this->getDiffDateYear($date,$compareDate);
        $days = $this->getDiffDateDay($date,$compareDate);
        $data = 0;
        if($difference > $data){
            $data = abs(floor(($difference - ($years * 31536000) - ($days * 86400)) / 3600));
        }

        return $data;
    }

    /**
     * function get Min diff date
     * @param string date
     * @param string $compareDate
     *
     * @return int
     **/
    public function getDiffDateMins($date,$compareDate)
    {
        $difference = $this->getTimeDiffDate($date,$compareDate);
        $years = $this->getDiffDateYear($date,$compareDate);
        $days = $this->getDiffDateDay($date,$compareDate);
        $hours = $this->getDiffDateHours($date,$compareDate);
        $data = 0;
        if($difference > $data){
            $data = abs(floor(($difference - ($years * 31536000) - ($days * 86400) - ($hours * 3600)) / 60));#floor($difference / 60);
        }

        return $data;
    }

    /**
     * @return Date\Day
     */
    public function getDay()
    {
        if(!$this->_day){
            include_once self::XML_PATH_DATE.'Day.php';
            $this->_day = new \WsoftPro\helpers\Wsoftpro\Date\Day();
        }
        return $this->_day;
    }

    /**
     * @param Date\Day $day
     * @return $this
     */
    public function setDay(\WsoftPro\helpers\Wsoftpro\Date\Day $day)
    {
        $this->_day = $day;
        return $this;
    }

    /**
     * @return Date\Year
     */
    public function getYear()
    {
        if(!$this->_year){
            include_once self::XML_PATH_DATE.'Year.php';
            $this->_year = new \WsoftPro\helpers\Wsoftpro\Date\Year();
        }
        return $this->_year;
    }

    /**
     * Function set Year helper
     *
     * @param Date\Year $year
     * @return $this
     */
    public function setYear(\WsoftPro\helpers\Wsoftpro\Date\Year $year)
    {
        $this->_year = $year;
        return $this;
    }

    /**
     * @return \WsoftPro\helpers\Wsoftpro\Date\Month
    **/
    public function getMonth()
    {
        if(!$this->_month){
            include_once self::XML_PATH_DATE.'Month.php';
            $this->_month = new \WsoftPro\helpers\Wsoftpro\Date\Month();
        }
        return $this->_month;
    }

    /**
     * Function set month helper
     *
     * @return \WsoftPro\helpers\Wsoftpro\Date
    **/
    public function setMonth(\WsoftPro\helpers\Wsoftpro\Date\Month $month)
    {
        $this->_month = $month;
        return $this;
    }

    /**
     * Function get Current date
    **/
    public function getCurrentlyDate()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $info = getdate();

        return $info;
    }
}