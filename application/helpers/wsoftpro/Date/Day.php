<?php
namespace WsoftPro\helpers\Wsoftpro\Date;
use WsoftPro\helpers\Wsoftpro\AbstractHelpers;

include_once '../AbstractHelpers.php';

class Day extends AbstractHelpers
{
    /**
     * A textual representation of a day, three letters
     * Mon through Sun
     *
     * @param $stringDate
     * @return string
    **/
    public function getDayWeek($stringDate)
    {
        return date('D', $this->dateToTime($stringDate));
    }

    /**
     * A full textual representation of the day of the week
     * Sunday through Saturday
     *
     * @param $stringDate
     * @return string
     **/
    public function getDayWeekFullName($stringDate)
    {
        return date('l', $this->dateToTime($stringDate));
    }

    /**
     * Numeric representation of the day of the week
     * 0 (for Sunday) through 6 (for Saturday)
     *
     * @param $stringDate
     * @return string
     **/
    public function getDayNumberInWeek($stringDate)
    {
        return date('w', $this->dateToTime($stringDate));
    }

    /**
     * The day of the year (starting from 0)
     * 0 through 365
     *
     * @param $stringDate
     * @return string
     **/
    public function getDayNumberInYear($stringDate)
    {
        return date('z', $this->dateToTime($stringDate));
    }

    /**
     * Day of the month without leading zeros
     * 1 to 31
     *
     * @param $stringDate
     * @return string
     **/
    public function getDaShortNumber($stringDate)
    {
        return date('j', $this->dateToTime($stringDate));
    }

    /**
     * Day of the month, 2 digits with leading zeros
     * 01 to 31
     *
     * @param $stringDate
     * @return string
     **/
    public function getDayFullNumber($stringDate)
    {
        return date('d', $this->dateToTime($stringDate));
    }


    /**
     * Function check string date be saturday
     *
     * @param string $stringDate
     * @return bool
    **/
    function isSaturday($stringDate) {
        $weekDay = $this->getDayNumberInWeek($stringDate);
        return ($weekDay == 6);
    }

    /**
     * Function check string date be Sunday
     *
     * @param string $stringDate
     * @return bool
     **/
    function isSunday($stringDate) {
        $weekDay = $this->getDayNumberInWeek($stringDate);
        return ($weekDay == 0);
    }
}