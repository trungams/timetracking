<?php
namespace WsoftPro\helpers\Wsoftpro\Date;
use WsoftPro\helpers\Wsoftpro\AbstractHelpers;

include_once '../AbstractHelpers.php';

class Year extends AbstractHelpers
{
    /**
     * A full numeric representation of a year, 4 digits
     *
     * @param $stringDate
     * @return string
    **/
    public function getFullYearNumber($stringDate)
    {
        return date('Y', $this->dateToTime($stringDate));
    }

    /**
     * A two digit representation of a year
     *
     * @param $stringDate
     * @return string
     **/
    public function getShortYearNumber($stringDate)
    {
        return date('y', $this->dateToTime($stringDate));
    }
}