<?php
namespace WsoftPro\helpers\Wsoftpro\Date;
use WsoftPro\helpers\Wsoftpro\AbstractHelpers;

include_once '../AbstractHelpers.php';

class Month extends AbstractHelpers
{
    /**
     * A full textual representation of a month, such as January or March
     * January through December
     *
     * @param $stringDate
     * @return string
    **/
    public function getFullTextMonth($stringDate)
    {
        return date('F', $this->dateToTime($stringDate));
    }

    /**
     * Number of days in the given month
     * 28 through 31
     *
     * @param $stringDate
     * @return string
     **/
    public function getDateNumberInMonth($stringDate)
    {
        return date('t', $this->dateToTime($stringDate));
    }

    /**
     * Numeric representation of a month, without leading zeros
     * 1 through 12
     *
     * @param $stringDate
     * @return string
     **/
    public function getSortMonthNumber($stringDate)
    {
        return date('n', $this->dateToTime($stringDate));
    }

    /**
     * Numeric representation of a month, with leading zeros
     * 01 through 12
     *
     * @param $stringDate
     * @return string
     **/
    public function getFullMonthNumber($stringDate)
    {
        return date('m', $this->dateToTime($stringDate));
    }

    /**
     * A short textual representation of a month, three letters
     * Jan through Dec
     *
     * @param $stringDate
     * @return string
     **/
    public function getFullMonthText($stringDate)
    {
        return date('M', $this->dateToTime($stringDate));
    }
}