<?php
namespace WsoftPro\helpers\Wsoftpro;

include_once 'AbstractHelpers.php';

class FormatHtml extends \WsoftPro\helpers\Wsoftpro\AbstractHelpers
{
    public function formatTextToHtml($text)
    {
        return str_replace("\r", "\n<br />", $text);
    }
}