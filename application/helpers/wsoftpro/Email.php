<?php
namespace WsoftPro\helpers\Wsoftpro;

include_once 'AbstractHelpers.php';

class Email extends \WsoftPro\helpers\Wsoftpro\AbstractHelpers
{

    /**@var \Mailbox_Model**/
    protected $_mailbox_model;

    protected $_fileAttacheMent;

    protected $_message_body;

    protected $_to;

    protected $_subject;

    /**
     * Construct
     *
     **/
    public function __construct($device = false)
    {
        parent::__construct($device);
        $this->_mailbox_model = $this->getModel('Mailbox');
    }

    /**
     * @return mixed
     */
    public function getFileAttacheMent()
    {
        return $this->_fileAttacheMent;
    }

    /**
     * @param mixed $fileAttacheMent
     * @return \WsoftPro\helpers\Wsoftpro\Email
     */
    public function setFileAttacheMent($fileAttacheMent)
    {
        $this->_fileAttacheMent = $fileAttacheMent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessageBody()
    {
        return $this->_message_body;
    }

    /**
     * @param mixed $message_body
     * @return \WsoftPro\helpers\Wsoftpro\Email
     */
    public function setMessageBody($message_body)
    {
        $this->_message_body = $message_body;
        return $this;
    }

    /**
     * @return \Mailbox_Model
     */
    public function getMailboxModel()
    {
        return $this->_mailbox_model;
    }

    /**
     * @param \Mailbox_Model $mailbox_model
     * @return \WsoftPro\helpers\Wsoftpro\Email
     */
    public function setMailboxModel($mailbox_model)
    {
        $this->_mailbox_model = $mailbox_model;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->_to;
    }

    /**
     * @param mixed $to
     * @return \WsoftPro\helpers\Wsoftpro\Email
     */
    public function setTo($to)
    {
        $this->_to = $to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->_subject;
    }

    /**
     * @param mixed $subject
     * @return \WsoftPro\helpers\Wsoftpro\Email
     */
    public function setSubject($subject)
    {
        $this->_subject = $subject;
        return $this;
    }

    public function sendEmail($user_id)
    {
        $all_email = $this->getTo();
        if($all_email && !is_array($all_email) && is_string($all_email)){
            $all_email = explode(',',$all_email);
        }

        // get all email address
        foreach ($all_email as $v_email) {
            try{
                $data = array();
                $data['subject'] = $this->getSubject();
                $data['message_body'] = $this->getMessageBody();
                if (!empty($this->getFileAttacheMent())) {
                    $val = $this->getFileAttacheMent();
                    // save into send table
                    $data['attach_filename'] = $val['fileName'];
                    $data['attach_file'] = $val['path'];
                    $data['attach_file_path'] = $val['fullPath'];
                    // save into inbox table
                    $idata['attach_filename'] = $val['fileName'];
                    $idata['attach_file'] = $val['path'];
                    $idata['attach_file_path'] = $val['fullPath'];
                } else {
                    $data['attach_filename'] = NULL;
                    $data['attach_file'] = NULL;
                    $data['attach_file_path'] = NULL;
                    // save into inbox table
                    $idata['attach_filename'] = NULL;
                    $idata['attach_file'] = NULL;
                    $idata['attach_file_path'] = NULL;
                }
                $data['to'] = $v_email;

                /*
                 * Email Configuaration
                 */
                $profile_info = $this->getMailboxModel()->check_by(array('user_id' => $user_id), 'tbl_account_details');
                $user_info = $this->getMailboxModel()->check_by(array('user_id' => $user_id), 'tbl_users');
                $mailbox = array('email' => $user_info->email, 'name' => $profile_info->fullname);

                // get company name
                $name = $profile_info->fullname;
                $info = $data['subject'];
                // set from email
                $from = array($name, $info);
                // set sender email
                $to = $v_email;
                //set subject
                $subject = $data['subject'];
                $data['user_id'] = $user_id;
                $data['message_time'] = date('Y-m-d H:i:s');
                // save into send
                $this->getMailboxModel()->_table_name = 'tbl_sent';
                $this->getMailboxModel()->_primary_key = 'sent_id';
                $send_id = $this->getMailboxModel()->save($data);
                // get mail info by send id to send
                $this->getMailboxModel()->_order_by = 'sent_id';
                $data['read_mail'] = $this->getMailboxModel()->get_by(array('sent_id' => $send_id), true);
                // set view page
                $message = $this->getMessageBody();

                $params['subject'] = $subject;
                $params['message'] = $message;
                $params['resourceed_file'] = '';
                $params['recipient'] = $data['to'];
                $send_email = $this->getMailboxModel()->send_email($params, $mailbox);

                // save into inbox table procees
                $idata['to'] = $data['to'];
                $idata['from'] = $user_info->email;
                $idata['user_id'] = $user_id;
                $idata['subject'] = $data['subject'];
                $idata['message_body'] = $data['message_body'];
                $idata['message_time'] = date('Y-m-d H:i:s');
                // save into inbox
                $this->getMailboxModel()->_table_name = 'tbl_inbox';
                $this->getMailboxModel()->_primary_key = 'inbox_id';
                $this->getMailboxModel()->save($idata);
                $activity = array(
                    'user' => $user_id,
                    'module' => 'mailbox',
                    'module_field_id' => $user_id,
                    'activity' => lang('activity_msg_sent'),
                    'icon' => 'fa-circle-o',
                    'value1' => $v_email
                );
                $this->getMailboxModel()->_table_name = 'tbl_activities';
                $this->getMailboxModel()->_primary_key = 'activities_id';
                $this->getMailboxModel()->save($activity);
            }catch (\Exception $e){
                $this->logMessage($e->getMessage());
            }
        }

        if ($send_email) {
            $type = "success";
            $message = lang('msg_sent');
            set_message($type, $message);
            redirect('admin/mailbox/index/sent');
        } else {
            show_error('Error send an email');
        }
    }

}