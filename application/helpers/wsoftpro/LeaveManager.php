<?php
namespace WsoftPro\helpers\Wsoftpro;

include_once 'AbstractHelpers.php';

class LeaveManager extends \WsoftPro\helpers\Wsoftpro\AbstractHelpers
{
    const XML_DEFAULT_LEAVE_DAYS_YEAR = 12;
    protected $_leaveDay;

    /**
     * @var \WsoftPro\helpers\Wsoftpro\Date
     **/
    protected $_date;

    /**
     * @var string
     **/
    protected $_dateString;

    /**
     * @return mixed
     */
    public function getDateString()
    {
        if(!$this->_dateString){
            $info = $this->getDate()->getCurrentlyDate();
            $date = $info['mday'];
            $month = $info['mon'];
            $year = $info['year'];
            $date = "$year-$month-$date";
            $this->_dateString = $date;
        }
        return $this->_dateString;
    }

    /**
     * @param mixed $dateString
     * @return \WsoftPro\helpers\Wsoftpro\LeaveManager
     */
    public function setDateString($dateString)
    {
        $this->_dateString = $dateString;
        return $this;
    }


    /**
     * @return Date
     */
    public function getDate()
    {
        return $this->_date;
    }

    public function __construct($device = false)
    {
        parent::__construct($device);
        $this->_date = $this->getHelper('Date');
    }

    public function canSelectLeaveSalary($user_id)
    {
        $leaveSalary = $this->getTotalLeaveSalary($user_id);
        if(count($leaveSalary) < self::XML_DEFAULT_LEAVE_DAYS_YEAR){
            return true;
        }

        return false;
    }

    public function getAllLeaveInYear($user_id)
    {
        $sql = "SELECT * FROM tbl_leave_application as appleav
                WHERE 
                    YEAR(appleav.leave_start_date) = '".$this->getYearNumber()."'
                    AND appleav.user_id = ".$user_id;

        $this->_leaveDay = $this->getAllResult($sql);

        return $this->_leaveDay;
    }

    public function getTotalLeaveSalary($user_id)
    {
        $leaveSalary = array();
        $allLeave = $this->getAllLeaveInYear($user_id);
        foreach ($allLeave as $leave){
            if($leave->approve_salary == 1){
                $leaveSalary[] = $leave;
            }
        }

        return $leaveSalary;
    }

    public function getTotalLeaveNoSalary($user_id)
    {
        $leaveNoSalary = array();
        $allLeave = $this->getAllLeaveInYear($user_id);
        foreach ($allLeave as $leave){
            if($leave->approve_salary != 1){
                $leaveNoSalary[] = $leave;
            }
        }

        return $leaveNoSalary;
    }

    public function getYearNumber()
    {
        return $this->getDate()->getYear()->getFullYearNumber($this->getDateString());
    }
}