<?php
namespace WsoftPro\helpers\Wsoftpro;

require_once 'Device.php';

abstract class AbstractHelpers
{
    const XML_BASE_PATH_HELPER_FILE = 'application/helpers/wsoftpro/';
    const XML_BASE_PATH_MODELS_FILE = 'application/models/';
    const XML_IP_COMPANY = '27.72.61.16';

    protected $_session;

    protected $ipCompany = array('27.72.100.127','27.72.61.16');
    /** @var \WsoftPro\helpers\Wsoftpro\Device **/
    protected $_device;

    /**
     * Function get all session
     * @return bool | array
    **/
    public function getSession()
    {
        if(!$this->_session){
            $this->_session = false;
        }

        return $this->_session;
    }

    /**
     * Function set session
     * @param \CI_Session $session
     *
     * @return $this
     **/
    public function setSession(\CI_Session $session)
    {
        if($session){
            $this->_session = $session;
        }

        return $this;
    }

    /**
     * Function get helper class
     * @param string | null $helper
     * @return mixed
     **/
    public function getHelper($helper = null)
    {
        try{
            if($helper){
                $helperClass = self::XML_BASE_PATH_HELPER_FILE.$helper.'.php';
                if (file_exists($helperClass)) {
                    require_once $helperClass;
                    /* note that if using double quotes, "\\namespacename\\classname" must be used */
                    $helper = '\\WsoftPro\helpers\\Wsoftpro\\'.$helper;
                    $class = new $helper();
                    return $class;
                }
            }
        }catch (\Exception $e){
            $this->logMessage($e->getMessage());
        }

        return $this;
    }

    /**
     * Function get helper class
     * @param string | null $model
     * @return mixed
     **/
    public function getModel($model = null)
    {
        try{
            if($model){
                $modelClassFile = self::XML_BASE_PATH_MODELS_FILE.$model.'_model.php';
                if (file_exists($modelClassFile)) {
                    require_once $modelClassFile;
                    /* note that if using double quotes, "\\namespacename\\classname" must be used */
                    $model = $model.'_model';
                    $class = new $model();
                    return $class;
                }
            }
        }catch (\Exception $e){
            $this->logMessage($e->getMessage());
        }

        return $this;
    }

    /**
     * get Client IP address
     *
     * @return string
     **/
    public function getCurrentIPAddress()
    {
        $CI = $this->getConnectData();
        return $CI->input->ip_address();
    }

    /**
     * Check with IP in my company
     * @param string | null $ip_address
     * @return bool
    **/
    public function checkIpCompany($ip_address = null)
    {
        try{
            if(!$ip_address){
                $ip_address = $this->getCurrentIPAddress();
            }
            if($ip_address === self::XML_IP_COMPANY || in_array($ip_address,$this->ipCompany)){
                return true;
            }
        }catch (\Exception $e){
            $this->logMessage($e->getMessage());
        }

        return false;
    }

    /**
     * @param string $stringDate
     * @param string $format
     * @return string
     **/
    public function formatDate($format,$stringDate)
    {
        return date($format, $this->dateToTime($stringDate));
    }

    /**
     * @param string $stringDate
     * @return int
    **/
    public function dateToTime($stringDate)
    {
        return strtotime($stringDate);
    }

    /**
     * Construct
     *
     * @param mixed $device
    **/
    public function __construct($device = false)
    {
        if(!$device && !$this->_device){
            $device = new \WsoftPro\helpers\Wsoftpro\Device();
        }
        $this->setDevice($device);
    }

    /**
     * @return \WsoftPro\helpers\Wsoftpro\Device
    */
    public function getDevice()
    {
        return $this->_device;
    }

    /**
     * @return \WsoftPro\helpers\Wsoftpro\AbstractHelpers
    **/
    public function setDevice(\WsoftPro\helpers\Wsoftpro\Device $device)
    {
        $this->_device = $device;

        return $this;
    }

    /**
     * Comare
    **/
    public function compareDevice($deviceData)
    {
        $compare = true;

        /**@var \User_Model**/
        $userModel = $this->getModel('User');
        if(isset($deviceData['user_id'])){
            $device = $userModel->getDeviceInfo($deviceData['user_id']);
            if($device){
                if($deviceData['device_osx'] !== $device->device_osx){
                    $compare = false;
                }
                if($deviceData['device_name'] !== $device->device_name){
                    $compare = false;
                }
                if($deviceData['device_user'] !== $device->device_user){
                    $compare = false;
                }
            }
        }

        return $compare;
    }

    /**
     * Function log message
    **/
    public function logMessage($message)
    {
        @file_put_contents('kevin-log.log',print_r(array($message),true),FILE_APPEND);
    }

    /**
     * Function get first row by collection query
     *
     * @param string $sql
     * @param array | null $condition
     * @return \stdClass | bool
    **/
    public function getFirstRow($sql,$condition = null)
    {
        try{
            $CI = $this->getConnectData();
            if($CI !== false){
                if(!is_array($condition)){
                    $condition = array();
                }
                $data = $CI->db->query($sql, $condition);
                if(count($data->result())){
                    return $data->first_row();
                }
            }
        }catch (\Exception $e){
            $this->logMessage($e->getMessage());
        }

        return false;
    }

    /**
     * Function get all row by collection query
     *
     * @param string $sql
     * @param array | null $condition
     * @return array | bool
     **/
    public function getAllResult($sql,$condition = null)
    {
        try{
            $CI = $this->getConnectData();
            if($CI !== false){
                if(!is_array($condition)){
                    $condition = array();
                }
                $data = $CI->db->query($sql, $condition);
                return $data->result();
            }
        }catch (\Exception $e){
            $this->logMessage($e->getMessage());
        }

        return false;
    }

    /**
     * Function get Data off object by keys
     * @return array |bool
     **/
    public function getAllDataObject($object,$keys)
    {
        $data = array();
        foreach ($keys as $key){
            $data[$key] = $this->getDataObject($object,$key);
        }

        return $data;
    }

    /**
     * Function get Data off object by key
     * @return mixed
    **/
    public function getDataObject($object,$key)
    {
        try{
            foreach ($object as $objKey=>$value){
                if($key === $objKey){
                    return $value;
                }
            }
        }catch (\Exception $e){
            $this->logMessage($e->getMessage());
        }

        return null;
    }

    /**
     * Function connection to Database
     * @return \CI_Controller | bool
    **/
    public function getConnectData()
    {
        try{
            $CI = &get_instance();
            return $CI;
        }catch (\Exception $e){
            $this->logMessage($e->getMessage());
        }

        return false;
    }
}