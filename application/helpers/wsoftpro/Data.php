<?php
namespace WsoftPro\helpers\Wsoftpro;

include_once 'AbstractHelpers.php';

class Data extends \WsoftPro\helpers\Wsoftpro\AbstractHelpers
{
    public function getUser($uname,$userID)
    {
        $sql = "SELECT * FROM tbl_emp_device WHERE device_name = '".$uname."' AND device_user = '".$userID."'";

        $data = $this->getFirstRow($sql);
        return $data;
    }

    public function autoCheckDevice($uname,$userID){
        $userDeviceData = $this->getUser($uname,$userID);
        $fullUser = $this->_device->getUserAgent();
        if($userDeviceData && $userDeviceData->device_full_name == $fullUser) {
            return $userDeviceData->user_id;
        }
        return false;
    }

    public function GetCurrentTimeServer()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $info = getdate();
        $date = $info['mday'];
        $month = $info['mon'];
        $year = $info['year'];
        $hour = $info['hours'];
        $min = $info['minutes'];
        $sec = $info['seconds'];
        $time = "$hour:$min:$sec";
        $date = "$year-$month-$date";
        return array('time'=>$time,'date'=>$date);
    }


    public function autoClockIn()
    {
        file_put_contents('kevin.log',print_r(array($_POST),true),FILE_APPEND);
    }

    public function getIpCompany()
    {
        $currentIp = $this->getDevice()->getClientIPAddress();
        $ipCty = config_item('company_ip');
        if($currentIp == $ipCty){
            return $currentIp;
        }
        return false;
    }
}