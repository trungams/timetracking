<?php
/**
 * Description of Project_Model
 *
 * @author NaYeM
 */
class Attendance_Report_model extends MY_Model{

    public $_table_name = 'tbl_attendance_report';
    public $_order_by;
    public $_primary_key = 'id';

    public function get_report_by_month($month,$user_id)
    {
        $sql = 'SELECT * FROM `tbl_attendance_report` WHERE `user_id` = ? AND `year` = ? AND `month` = ?';
        $data = $this->db->query($sql, array($user_id, date('Y'), $month));

        return $data->result();
    }

}
