<?php

/**
 * Description of Knowledge base model
 *
 * @author NaYeM
 */
class Kb_model extends MY_Model
{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    function increase_total_view($id)
    {
        $tbl_knowledgebase = $this->db->dbprefix('tbl_knowledgebase');

        $sql = "UPDATE $tbl_knowledgebase
        SET total_view = total_view+1
        WHERE $tbl_knowledgebase.kb_id=$id";

        return $this->db->query($sql);
    }

    function get_suggestions($search)
    {
        $tbl_knowledgebase = $this->db->dbprefix('tbl_knowledgebase');
        $tbl_kb_category = $this->db->dbprefix('tbl_kb_category');

        $sql = "SELECT $tbl_knowledgebase.kb_id, $tbl_knowledgebase.title
        FROM $tbl_knowledgebase
        LEFT JOIN $tbl_kb_category ON $tbl_kb_category.kb_category_id=$tbl_knowledgebase.kb_category_id
        WHERE $tbl_knowledgebase.status='1' AND $tbl_kb_category.status='1'
            AND $tbl_knowledgebase.title LIKE '%$search%'
        ORDER BY $tbl_knowledgebase.title ASC
        LIMIT 0, 10";

        $result = $this->db->query($sql)->result();

        $result_array = array();
        foreach ($result as $value) {
            $result_array[] = array("value" => $value->kb_id, "label" => $value->title);
        }

        return $result_array;
    }
}
