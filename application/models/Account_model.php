<?php
/**
 * Description of Project_Model
 *
 * @author NaYeM
 */
class Account_Model extends MY_Model{

    public $_table_name;
    public $_order_by;
    public $_primary_key;


    public function getAccountInfor($user_id)
    {
        $sql = 'SELECT * FROM `tbl_users` WHERE `user_id` = ?';
        $data = $this->db->query($sql, array($user_id));

        return $data->result();
    }
}
