<div class="row">

    <div class="col-md-6">
        <div id="panelChart5" class="panel panel-custom">
            <div class="panel-heading">
                <div class="panel-title"><?= lang('task') . ' ' . lang('report') ?></div>
            </div>
            <div class="panel-body">
                <div class="chart-pie flot-chart"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">

        <div id="panelChart4" class="panel panel-custom">
            <div class="panel-heading">
                <div class="panel-title"><?= lang('total') . ' ' . lang('task') . ' ' . lang('time_spent') ?></div>
            </div>
            <div class="panel-body">
                <div class="form-group col-sm-12">
                    <?php
                    $tasks_info = $this->report_model->get_permission('tbl_task');
                    $task_time = 0;
                    if (!empty($tasks_info)) {
                        foreach ($tasks_info as $v_tasks) {
                            $task_time += $this->report_model->task_spent_time_by_id($v_tasks->task_id);
                        }
                    }
                    echo $this->report_model->get_time_spent_result($task_time)

                    ?>

                </div>
            </div>
        </div>
    </div>
    <!-- END row-->

    <div class="col-lg-12">
        <div class="panel panel-custom">
            <div class="panel-heading"><?= lang('tasks_r_assignment') ?></div>
            <div class="panel-body">
                <div id="morris-bar"></div>
            </div>
        </div>
    </div>
</div>


<?php
$not_started = 0;
$in_progress = 0;
$completed = 0;
$assign_for_client_testing = 0;
$waiting_for_code_review = 0;
$testing = 0;

if (!empty($all_tasks)):foreach ($all_tasks as $v_tasks):
    if ($v_tasks->task_status == 'not_started') {
        $not_started += count($v_tasks->task_status);
    }
    if ($v_tasks->task_status == 'in_progress') {
        $in_progress += count($v_tasks->task_status);
    }
    if ($v_tasks->task_status == 'completed') {
        $completed += count($v_tasks->task_status);
    }
    if ($v_tasks->task_status == 'assign_for_client_testing') {
        $assign_for_client_testing += count($v_tasks->task_status);
    }
    if ($v_tasks->task_status == 'waiting_for_code_review') {
        $waiting_for_code_review += count($v_tasks->task_status);
    }
	if ($v_tasks->task_status == 'testing') {
		$testing += count($v_tasks->task_status);
	}
endforeach;
endif;


?>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.tooltip.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.resize.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.pie.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.time.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.categories.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.spline.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var chartdata = [
                <?php if(!empty($user_tasks)):foreach($user_tasks as $user => $v_task_user):
                if($user != 'all'){
                if(!empty($assign_user)){foreach($assign_user as $v_user){
                if($v_user->user_id == $user){
                ?>
                {
                    y: "<?= $v_user->username?>",
                    <?php
                    $inparogress = 0;
                    $notstarted = 0;
                    $assign_for_client_testing = 0;
	                $waiting_for_code_review = 0;
	                $testing = 0;
	                $completed = 0;

                    foreach ($v_task_user as $status => $value) {
                        if ($status == 'not_started') {
                            $notstarted = count($value);
                        } elseif ($status == 'in_progress') {
                            $inparogress = count($value);
                        } elseif ($status == 'assign_for_client_testing') {
                            $assign_for_client_testing = count($value);
                        }elseif ($status == 'completed') {
	                        $completed = count($value);
                        }elseif ($status == 'waiting_for_code_review') {
                            $waiting_for_code_review = count($value);
                        }elseif ($status == 'testing') {
	                        $testing = count($value);
                        }
                    }
                    ?>
                    a:<?= $notstarted;?> ,
                    b: <?= $inparogress?>,
                    c: <?= $assign_for_client_testing?>,
                    d: <?= $completed?>,
                    e: <?= $waiting_for_code_review?>,
                    f: <?= $testing?>}

                <?php
                }
                }
                };
                }
                endforeach;
                endif;

                ?>
            ]
            ;
        new Morris.Bar({
            element: 'morris-bar',
            data: chartdata,
            xkey: 'y',
            ykeys: ["a", "b", "c","d","e","f"],
            labels: [
                "<?= lang('not_started')?>",
                "<?= lang('in_progress')?>",
                "<?= lang('assign_for_client_testing')?>",
                "<?= lang('completed')?>",
                "<?= lang('waiting_for_code_review')?>",
                "<?= lang('testing')?>"
            ],
            xLabelMargin: 2,
            barColors: ['#0083e5', '#902fff', '#00c220', '#f00013', '#fff900','#1c22ff'],
            resize: true,
            parseTime: false,
        });

        // CHART PIE
        // -----------------------------------
        (function (window, document, $, undefined) {

            $(function () {

                var data = [{
                    "label": "<?= lang('not_started')?>",
                    "color": "#0083e5",
                    "data": <?= $not_started?>
                }, {
                    "label": "<?= lang('in_progress')?>",
                    "color": "#902fff",
                    "data": <?= $in_progress?>
                }, {
                    "label": "<?= lang('completed')?>",
                    "color": "#00c220",
                    "data": <?= $completed?>
                }, {
                    "label": "<?= lang('assign_for_client_testing')?>",
                    "color": "#f00013",
                    "data": <?= $assign_for_client_testing?>
                }, {
                    "label": "<?= lang('waiting_for_code_review')?>",
                    "color": "#fff900",
                    "data": <?= $waiting_for_code_review?>
                },{
                    "label": "<?= lang('testing')?>",
                    "color": "#1c22ff",
                    "data": <?= $testing?>
                },];

                var options = {
                    series: {
                        pie: {
                            show: true,
                            innerRadius: 0,
                            label: {
                                show: true,
                                radius: 0.8,
                                formatter: function (label, series) {
                                    return '<div class="flot-pie-label">' +
                                            //label + ' : ' +
                                        Math.round(series.percent) +
                                        '%</div>';
                                },
                                background: {
                                    opacity: 0.8,
                                    color: '#222'
                                }
                            }
                        }
                    }
                };

                var chart = $('.chart-pie');
                if (chart.length)
                    $.plot(chart, data, options);

            });

        })(window, document, window.jQuery);

    });

</script>