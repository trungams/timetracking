<div class="panel panel-custom" data-collapsed="0">
    <div class="panel-heading">
        <div class="panel-title">
            <strong><?= $title ?></strong>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                    class="sr-only">Close</span></button>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="text-center ">
                <img src="<?php echo base_url().$profile_info->avatar; ?>" class="img-thumbnail img-circle thumb128 " />
            </div>
            <br />
            <br />
        </div>
        <div class="row">
            <div class="text-center col-sm-4">
                <h4><label class="control-label">total leave days</label></h4>
                <strong><?= $report_info->total_leave_days.' d' ?></strong>
            </div>
            <div class="text-center col-sm-4">
                <h4><label class="control-label">Leave Amount</label></h4>
                <strong><?= $report_info->leave_amounts .' VND'?></strong>
            </div>
            <div class="text-center col-sm-4">
                <h4><label class="control-label">Fine Amount</label></h4>
                <strong><?= $report_info->fine_amounts .' VND'?></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>User Information</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">Username</div>
                    <div class="col-sm-6"><?= $profile_info->fullname ?></div>
                </div>
                <div class="row">
                    <div class="col-sm-6">Joining Date</div>
                    <div class="col-sm-6"><?= $profile_info->joining_date ?></div>
                </div>
                <div class="row">
                    <div class="col-sm-6">Date Of Birth</div>
                    <div class="col-sm-6"><?= $profile_info->date_of_birth ?></div>
                </div>
                <div class="row">
                    <div class="col-sm-6">Fathers Name:</div>
                    <div class="col-sm-6"><?= $profile_info->father_name ?></div>
                </div>
                <div class="row">
                    <div class="col-sm-6">Mothers Name:</div>
                    <div class="col-sm-6"><?= $profile_info->mother_name ?></div>
                </div>
                <div class="row">
                    <div class="col-sm-6">Phone</div>
                    <div class="col-sm-6"><?= $profile_info->phone ?></div>
                </div>
                <div class="row">
                    <div class="col-sm-6">Skype id</div>
                    <div class="col-sm-6"><?= $profile_info->skype ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Timetracking Information</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">All Days working in a month</div>
                    <div class="col-sm-6"><?= $report_info->all_day_work ?> d</div>
                </div>
                <div class="row">
                    <div class="col-sm-6">days Worked</div>
                    <div class="col-sm-6"><?= $report_info->day_work ?> d</div>
                </div>
                <div class="row">
                    <div class="col-sm-6">Total hour worked</div>
                    <div class="col-sm-6"><?= $report_info->day_total_hh ?> h</div>
                </div>
                <div class="row">
                    <div class="col-sm-6">Time late:</div>
                    <?php $timeLate = get_time_diff($report_info->time_late); ?>
                    <div class="col-sm-6"><?= $timeLate['hour'].' h '.$timeLate['min'] ?></div>
                </div>
            </div>
        </div>
        <?php if(count($leave_info)): ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="text-center col-sm-12">
                            <h3>Leave Information</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="text-center col-sm-12">
                                    <h4>Start dates</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="text-center col-sm-12">
                                    <h4>total dates</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="text-center col-sm-12">
                                    <h4>End dates</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $totalDays = 0; ?>
                    <?php foreach ($leave_info as $leave): ?>
                        <?php $totalDays += (float)$leave->total_leave_days ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="col-sm-6">Start date</div>
                                    <div class="col-sm-6"><?= $leave->leave_start_date ?></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="col-sm-6">days</div>
                                    <div class="col-sm-6"><?= $leave->total_leave_days ?></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="col-sm-6">End Date</div>
                                    <div class="col-sm-6"><?= $leave->leave_end_date ?></div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="row">
                        <div class="col-sm-6"><h3><?= 'Total Days Leave apply' ?></h3></div>
                        <div class="col-sm-6"><h3><?= $totalDays. ' d' ?></h3></div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <?php
            $dataReports = json_decode($report_info->data_report_json,true);
            $dataDayWork = json_decode($report_info->day_work_in_month,true);
                $dataReport = $dataReports[2018][1];
            ?>
            <h4>All day not clock in</h4>
            <?php foreach ($dataDayWork as $key=> $value): ?>
                <?php if(!isset($dataReport[$key])): ?>
                    <div class="row">
                        <div class="col-sm-12"><?= $key ?></div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            <h4>All day clock in Error</h4>
            <?php foreach ($dataReport as $key=> $value): ?>
                <?php if($value['day_error']): ?>
                    <div class="row">
                        <div class="col-sm-12"><?= $key ?></div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>
