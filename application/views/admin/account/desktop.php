<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <div class="form-group">
                <label for="json">Sortable Panels</label>
                <textarea id="json" class="form-control"></textarea>
            </div>

        </div>
    </div>
</div>
<div id="widgets">
    <div id="1" class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">1 - Site Bilgileri</div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt>Sahibi</dt>
                    <dd>John Doe</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Domain</dt>
                    <dd>www.johndoe.com</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Başlangıç Tarihi</dt>
                    <dd>12.04.2016</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Bitiş Tarihi</dt>
                    <dd>12.04.2016</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Kalan Süre</dt>
                    <dd>365 Gün</dd>
                </dl>

            </div>
        </div>
    </div>
    <div id="2" class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">2 - Site Bilgileri</div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt>Sahibi</dt>
                    <dd>John Doe</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Domain</dt>
                    <dd>www.johndoe.com</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Başlangıç Tarihi</dt>
                    <dd>12.04.2016</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Bitiş Tarihi</dt>
                    <dd>12.04.2016</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Kalan Süre</dt>
                    <dd>365 Gün</dd>
                </dl>

            </div>
        </div>
    </div>
    <div id="3" class="col-lg-4">
        <div class="panel panel-primary">
            <div class="panel-heading">3 - Site Bilgileri</div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt>Sahibi</dt>
                    <dd>John Doe</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Domain</dt>
                    <dd>www.johndoe.com</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Başlangıç Tarihi</dt>
                    <dd>12.04.2016</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Bitiş Tarihi</dt>
                    <dd>12.04.2016</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Kalan Süre</dt>
                    <dd>365 Gün</dd>
                </dl>

            </div>
        </div>
    </div>
    <div id="4" class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">4 - Site Bilgileri</div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt>Sahibi</dt>
                    <dd>John Doe</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Domain</dt>
                    <dd>www.johndoe.com</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Başlangıç Tarihi</dt>
                    <dd>12.04.2016</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Bitiş Tarihi</dt>
                    <dd>12.04.2016</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Kalan Süre</dt>
                    <dd>365 Gün</dd>
                </dl>

            </div>
        </div>
    </div>

</div>
<div class="column col-sm-6 c1">
    <div id="2" class="panel panel-danger">
        <div class="panel-heading">Feeds</div>
        <div class="panel-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
    </div>
    <div id="3" class="panel panel-danger">
        <div class="panel-heading">News</div>
        <div class="panel-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
    </div>
</div>
<div class="column col-sm-12 c2">
    <div id="4" class="panel panel-danger">
        <div class="panel-heading">Shopping</div>
        <div class="panel-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
    </div>
</div>
<div class="column col-sm-6 c3">
    <div id="5" class="panel panel-danger">
        <div class="panel-heading">Links</div>
        <div class="panel-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
    </div>
</div>
<script src="<?= base_url() ?>assets/plugins/jquery-ui/jquery-u.js"></script>
<script type="text/javascript">
    $("#widgets").sortable({
        handle: ".panel",
        cursor: "move",
        opacity: 0.5,
        stop: function (event, ui) {
            $("#json").val(
                JSON.stringify(
                    $("#widgets").sortable(
                        'toArray',
                        {
                            attribute: 'id'
                        }
                    )
                )
            );
        }
    });
    $(function () {
        $(".column").sortable({
            connectWith: ".column",
            placeholder: 'ui-state-highlight',
            forcePlaceholderSize: true,
            stop: function (event, ui) {
                $("#json").val(
                    JSON.stringify(
                        $(".column").sortable(
                            'toArray',
                            {
                                attribute: 'id'
                            }
                        )
                    )
                );
            }
        });
        $(".column").disableSelection();
    });
</script>